module basic.picture_basic {
    requires javafx.controls;
    requires javafx.fxml;


    opens basic.picture_basic to javafx.fxml;
    exports basic.picture_basic;
    exports logic;
    opens logic to javafx.fxml;
}