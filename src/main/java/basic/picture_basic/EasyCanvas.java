package basic.picture_basic;
import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import logic.Picture;


public class EasyCanvas extends Application {

    private final int WIDTH = 300;
    private final int HEIGHT = 300;
    private final String TITLE = "Shapes";



    @Override
    public void start(Stage stage) {
        initUI(stage);
    }

    private void initUI(Stage stage) {

        Picture picture = new Picture(WIDTH,HEIGHT);
        picture.initDraw();
        var scene = new Scene(picture.getPane(), WIDTH, HEIGHT);
        stage.setTitle(TITLE);
        stage.setScene(scene);
        stage.show();


    }




}