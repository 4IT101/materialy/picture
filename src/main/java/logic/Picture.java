package logic;

import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class Picture {

    private Pane pane;
    private Canvas canvas;
    private int width;
    private int height;

    public Picture(int width, int height) {
        this.width = width;
        this.height = height;
        this.pane = new Pane();
        this.canvas = new Canvas(this.width, this.height);
    }

    public void initDraw(){
        var gc = canvas.getGraphicsContext2D();
        drawShapes(gc);

        final Rectangle rect1 = new Rectangle(10, 10, 100, 100);
        rect1.setArcHeight(20);
        rect1.setArcWidth(20);
        rect1.setFill(Color.RED);

        FadeTransition ft = new FadeTransition(Duration.millis(3000), rect1);
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
        ft.setCycleCount(Timeline.INDEFINITE);
        ft.setAutoReverse(true);
        ft.play();


        drawComponents(canvas,rect1);
    }

    public static void drawShapes(GraphicsContext gc) {
        gc.setFill(Color.GRAY);
        gc.fillOval(30, 30, 50, 50);
        gc.fillOval(110, 30, 80, 50);
        gc.fillRect(220, 30, 50, 50);
        gc.fillRoundRect(30, 120, 50, 50, 20, 20);
        gc.fillArc(110, 120, 60, 60, 45, 180, ArcType.OPEN);
        gc.fillPolygon(new double[]{220, 270, 220},
                new double[]{120, 170, 170}, 3);
    }



    public void draw(){
        //todo
    }



    public Pane getPane() {
        return pane;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void drawComponents(Node... e){
        pane.getChildren().addAll(e);
    }

}


