package logic;


import basic.picture_basic.EasyCanvas;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class Main {

    public static void main(String[] args) {
                Application.launch(EasyCanvas.class, args);
    }
}
